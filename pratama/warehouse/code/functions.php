<?php
//c rack
//b shelf
//a cabinet/drawer

function switchSWLText($swl, $ton = true) {
  if (is_null($swl)) {
    return '?';
  } elseif ($swl == 'wip') {
    return 'WIP';
  } elseif ($swl == 'inf') {
    return '&infin;';
  } elseif ($swl == 'vary') {
    return 'vary';
  } else {
    return reformatWeightForEcho($swl);
  }
}

function reformatWeightForEcho($number, $abs = false) {
  $num2 = abs($number);
  $unit = '';
  if ($num2 == 0) {
  } elseif ($num2 < 1) {
    $number *= 1000;
    $unit    = ' g';
  } elseif ($num2 < 1000) {
    $unit = ' kg';
  } else {
    $number /= 1000;
    $unit    = ' T';
  }

  if ($abs) { return abs(round($number, 2)) . $unit; } else { return round($number, 2) . $unit; }
}

function switchCategoryName($what) {
  switch ($what) {
    case 'golden_item': return 'Golden Item';
    case 'consumable' : return 'Consumable';
    case 'non_cat'    : return 'NON-CAT';
    case 'hazard'     : return 'Dangerous Goods';
    default           : return '';
  }
}

function parseBin($bin) {
  if (is_string($bin)) {
    $arr        = array();
    $arr['bin'] = $bin = strtoupper($bin);

    // C     L 1 3   A 0 2
    // 0     1 2 3   4 5 6

    // c 1   b 2 3   a 0 1
    // 0 1   2 3 4   5 6 7
    // ^ ^   ^ ^ ^   ^ ^-^-container
    // | |   | | |   ^-----slot
    // | |   | | |
    // | |   | ^-^---------stack
    // | |   ^-------------lane
    // | |
    // | ^-----------------group_id
    // ^-------------------group

    if (preg_match('/^[ABC][0-9][A-Z][0-9]{2}/', $bin)) {
      $arr['group']     = substr($bin, 0, 1);
      $arr['group_id']  = substr($bin, 1, 1);
      $arr['lane']      = substr($bin, 2, 1);
      $arr['stack']     = substr($bin, 3, 2);
      $slot             = substr($bin, 5, 1);
      $arr['slot']      = $slot ? $slot : '~';
      $container        = substr($bin, 6, 2);
      $arr['container'] = $container ? $container : '00';
    } elseif (preg_match('/^[ABC][A-Z][0-9]{2}/', $bin)) {
      $arr['group']     = substr($bin, 0, 1);
      $arr['group_id']  = '0';
      $arr['lane']      = substr($bin, 1, 1);
      $arr['stack']     = substr($bin, 2, 2);
      $slot             = substr($bin, 4, 1);
      $arr['slot']      = $slot ? $slot : '~';
      $container        = substr($bin, 5, 2);
      $arr['container'] = $container ? $container : '00';
    } else {
      $arr['group']
          = $arr['group_id']
          = $arr['lane']
          = $arr['stack']
          = $arr['slot']
          = $arr['container']
          = '';
    }

    return $arr;
  } else {
    return null;
  }
}

function parseItem($value) {
  $arr                = array();
  $bin                = $value['Bin'];

  $arr['st']          = $value['ST'];
  $arr['sos']         = $value['SOS'];
  $arr['part_number'] = $value['PART NO.'];
  $arr['description'] = $value['DESCRIPTION'];
  $arr['pack']        = $value['Pack'];
  $arr['oh']          = $value['OH'];
  $arr['oo']          = $value['OO'];
  $arr['ip']          = $value['IP'];
  $arr['min']         = $value['MIN'];
  $arr['max']         = $value['MAX'];
  //$arr['min_2']       = $value['MIN_2'];
  $arr['stk']         = $value['STK'];
  //$arr['cc']          = $value['CC'];
  //$arr['act']         = $value['Act'];
  //$arr['landed']      = $value['Landed'];

  $arr['ta']          = $arr['oh'] + $arr['oo'] + $arr['ip'];
  $arr['mt']          = max($arr['max'], $arr['ta']);

  $arr['weight']      = $value['Weight'] ? 0 + $value['Weight'] : 0.001;
  $arr['bin']         = $bin;

  unset($arr['st']);
  unset($arr['sos']);
  unset($arr['part_number']);
  unset($arr['description']);
  unset($arr['pack']);
  unset($arr['oo']);
  unset($arr['ip']);
  unset($arr['min']);
  unset($arr['max']);
  unset($arr['stk']);

  $swl = parseBin($bin);
  if (!is_null($swl)) {
    $arr = array_merge($arr, $swl);
  }
  return $arr;
}

class MySorter {
  public $sort_list;

  function __construct()
  {
    $this->sort_list = array();
    $this->loop('./../data/sort.csv');
  }
  private function loop($file)
  {
    $f = fopen($file, 'r');
    while (($v=fgetcsv($f))!==FALSE) {
      $this->sort_list[ $v[0] ] = $v[1];
    }
    fclose($f);
  }
}

class SWL {
  //public $swl_list;
  public $swl;
  private $bin_list;

  function __construct()
  {
    $this->swl = array();
    $this->bin_list = array();
    $this->loop('./../data/swl_w1a.csv');
    $this->loop('./../data/swl_w1b.csv');
    $this->loop('./../data/swl_w1c.csv');
    $this->loop('./../data/swl_w2c.csv');
    $this->loop('./../data/swl_w3b.csv');
    $this->loop('./../data/swl_w3c.csv');
  }
  private function loop($file)
  {
    // C1A13A
    $f = fopen($file, 'r');
    while (($v=fgetcsv($f))!==FALSE) {
      $this->bin_list[ strtoupper($v[0]) ] = null;

      $arr = parseBin($v[0]);
      $arr['swl'] = $v[1];
      $arr['category'] = isset($v[2]) ? $v[2] : '';
      $this->swl[] = $arr;
    }
    fclose($f);
  }

  function swl_exist($bin)
  {
    if (preg_match('/^[ABC][0-9][A-Z][0-9]{2}/', $bin)
        && array_key_exists(strtoupper(substr($bin, 0, 6)), $this->bin_list)) {
      return true;
    } elseif (preg_match('/^[ABC][A-Z][0-9]{2}/', $bin)
              && array_key_exists(strtoupper(substr($bin, 0, 5)), $this->bin_list)) {
      return true;
    } else {
      return false;
    }
  }
}

class Bin {
  public $bin;

  function __construct()
  {
    $this->bin = array();
    $this->loop('./../data/data.csv');
  }
  private function loop($file)
  {
    $f = fopen($file, 'r');
    $keys = fgetcsv($f);
    while (($values=fgetcsv($f))!==FALSE) {
      $arr = array_combine($keys, $values);
      $this->bin[] = parseItem($arr);
    }
    fclose($f);
  }
}



class Container
{
  public $bin;
  public $item;

  function __construct()
  {
    $this->bin = '';
    $this->item = array();
  }
  function __toString()
  {
    return "{$this->bin}";
  }
  function update_item($v)
  {
    $this->item[] = $v;
  }

  function get_weight_data($what)
  {
    return $this->get_x_data('weight', $what);
  }

  function get_x_data($what1, $what2)
  {
    $ret = '';
    foreach ($this->item as $v) {
      $ret += $v[$what1] * $v[$what2];
    }
    return $ret;
  }
}

class Slot
{
  public $bin;
  public $swl;
  public $container;

  function __construct()
  {
    $this->bin = '';
    $this->swl = null;
    $this->container = array();
  }

  function update_swl($v)
  {
    $this->bin = $v['group'] . ($v['group_id'] == '0' ? '' : $v['group_id']) . $v['lane'] . $v['stack'] . $v['slot'];
    $this->swl = $v['swl'];
  }
  function update_item($v)
  {
    $this->bin = $v['group'] . ($v['group_id'] == '0' ? '' : $v['group_id']) . $v['lane'] . $v['stack'] . $v['slot'];
    if (!is_null($this->container) && array_key_exists($v['container'], $this->container)) {
      $this->container[$v['container']]->update_item($v);
    } else {
      $item = new Container;
      $item->update_item($v);
      $this->container[$v['container']] = $item;
    }
  }

  function echo_data()
  {
    $slot   = substr($this->bin, -1);
    $slot   = $slot == '~' ? 'TTL' : $slot;
    $str    = '';

    if (count($this->container) != 0) {
      $str  .= '<td class="cell-text cell-bin-name">' . $slot . '</td>';
      if ($this->swl == 'inf') {
        $str .= '<td class="cell-number cell-available has-mt-oh reformat-absolute switch-data switch-title special-a"
                     title="..."
                     data-total-mt="' . ($this->get_weight_data('mt') * 1000) . '"
                     data-total-oh="' . ($this->get_weight_data('oh') * 1000) . '"
                     data-each-mt="' . $this->echo_each_weight_data('mt') . '"
                     data-each-oh="' . $this->echo_each_weight_data('oh') . '"
                     >...</td>';
      } elseif ($this->swl == 'vary') {
        $str .= '<td class="cell-number cell-available has-mt-oh reformat-absolute switch-data switch-title"
                     title="..."
                     data-total-mt="' . ($this->get_weight_data('mt') * 1000) . '"
                     data-total-oh="' . ($this->get_weight_data('oh') * 1000) . '"
                     data-each-mt="' . $this->echo_each_weight_data('mt') . '"
                     data-each-oh="' . $this->echo_each_weight_data('oh') . '"
                     >...</td>';
      } else {
        $str .= '<td class="cell-number cell-available has-mt-oh switch-data switch-title switch-swl switch-percent"
                     title="..."
                     data-swl="' . ($this->swl * 1000) . '"
                     data-percent-mt="' . $this->get_percentage('mt') . '"
                     data-percent-oh="' . $this->get_percentage('oh') . '"
                     data-total-mt="' . ($this->get_weight_data('mt') * 1000) . '"
                     data-total-oh="' . ($this->get_weight_data('oh') * 1000) . '"
                     data-each-mt="' . $this->echo_each_weight_data('mt') . '"
                     data-each-oh="' . $this->echo_each_weight_data('oh') . '"
                     >...</td>';
      }
      $str .= '<td class="cell-number cell-swl">' . str_replace(' kg', 'kg', switchSWLText($this->swl)) . '</td>';

    } else {
      $str .= '<td class="cell-text cell-bin-name">' . $slot . '</td>';

      if (is_numeric($this->swl)) {
        $str .= '<td class="cell-number cell-available has-mt-oh switch-data switch-swl"
                     title="empty"
                     data-swl="' . ($this->swl * 1000) . '"
                     data-percent-mt="0"
                     data-percent-oh="0"
                     data-total-mt="0"
                     data-total-oh="0"
                     >...</td>';
      } elseif ($this->swl == 'vary') {
        $str .= '<td class="cell-number cell-available switch-data switch-percent"
                     title="empty"
                     data-percent-mt="' . $this->get_percentage('mt') . '"
                     data-percent-oh="' . $this->get_percentage('oh') . '"
                     data-total-mt="' . $this->get_weight_data('mt') . '"
                     data-total-oh="' . $this->get_weight_data('oh') . '"
                     >0</td>';
      } else {
        $str .= '<td class="cell-number cell-available switch-data switch-percent"
                     title="empty"
                     data-percent-mt="' . $this->get_percentage('mt') . '"
                     data-percent-oh="' . $this->get_percentage('oh') . '"
                     data-total-mt="' . $this->get_weight_data('mt') . '"
                     data-total-oh="' . $this->get_weight_data('oh') . '"
                     ><em>' . switchSWLText($this->swl, false) . '</em></td>';
      }
      $str .= '<td class="cell-number cell-swl">' . str_replace(' kg', 'kg', switchSWLText($this->swl)) . '</td>';
    }

    return $str;  
  }

  function get_weight_data($what)
  {
    $ret = '';
    foreach ($this->container as $v) {
      $ret += $v->get_weight_data($what);
    }
    return $ret;
  }
  function get_each_weight_data($what)
  {
    $each   = array();
    $sorted = $this->container;
    ksort($sorted);
    foreach ($sorted as $k => $v) {
      $each[$k] = $v->get_weight_data($what);
    }

    return $each;
  }
  function echo_each_weight_data($what)
  {
    $each = '';
    foreach ($this->get_each_weight_data($what) as $k => $v) {
      if ($each === '') {
        $each .= $k . ':' . ($v * 1000);
      } else {
        $each .= ";" . $k . ':' . ($v * 1000);
      }
    }

    return $each;
  }
  function title_echo_each_weight_data($what)
  {
    $each = '';
    foreach ($this->get_each_weight_data($what) as $k => $v) {
      if ($each === '') {
        $each .= '[' . $k . ': ' . str_replace(' ', '&nbsp;', reformatWeightForEcho($v)) . ']';
      } else {
        $each .= "; [" . $k . ': ' . str_replace(' ', '&nbsp;', reformatWeightForEcho($v)) . ']';
      }
    }

    return $each;
  }

  function get_percentage($what) {
    if (count($this->container) != 0 && is_numeric($this->swl)) {
      return 100 * $this->get_weight_data($what) / $this->swl;
    }
    return -1;
  }
}

class Stack
{
  public $bin;
  public $slot;

  public $category;

  function __construct()
  {
    $this->bin      = '';
    $this->slot     = array();
    $this->category = '';
  }

  function update_swl($v)
  {
    if (!$this->category) { $this->category = $v['category']; }
    $this->bin = $v['group'] . ($v['group_id'] == '0' ? '' : $v['group_id']) . $v['lane'] . $v['stack'];
    if (!is_null($this->slot) && array_key_exists($v['slot'], $this->slot)) {
      $this->slot[$v['slot']]->update_swl($v);
    } else {
      $item = new Slot;
      $item->update_swl($v);
      $this->slot[$v['slot']] = $item;
    }
  }
  function update_item($v)
  {
    $this->bin = $v['group'] . ($v['group_id'] == '0' ? '' : $v['group_id']) . $v['lane'] . $v['stack'];
    if (!is_null($this->slot) && array_key_exists($v['slot'], $this->slot)) {
      $this->slot[$v['slot']]->update_item($v);
    } else {
      $item = new Slot;
      $item->update_item($v);
      $this->slot[$v['slot']] = $item;
    }
  }

  function echo_data()
  {
    $slot = $this->slot;
    if (array_key_exists('~', $this->slot)) {
      $slot = $this->reupdate_swl();
    }

    $sorted = $slot;
    ksort($sorted);
    $category = ' class="item-' . ( $this->category == '' ? 'regular' : $this->category ) . '"';

    $str = '';

    $str .= '<table class="trakindo-table">';
    $str .= '<caption'.$category.'>' . $this->bin . '</caption>';
    $str .= '<thead'.$category.'>';
    $str .= '<th class="cell-location" title="Location">Loc.</th>';

    $str .= '<th class="cell-total">Wh/Rem</th>';

    $str .= '<th class="cell-swl">SWL</th>';
    $str .= '</thead>';
    $str .= '<tbody>';
    foreach (array_reverse($sorted) as $v) {
      $str .= '<tr>' . $v->echo_data() . '</tr>';
      if (substr($v->bin, -1) == '~') {
        $str .= '<tr class="shelf-total-header" data-hide-on-filter="true"><td colspan="3" style="text-align:center">Weight</td></tr>';
      }
    }
    $str .= '</tbody>';
    $str .= '</table>';
    return $str . "\n";
  }

  private function reupdate_swl()
  {
    $slot = $this->slot;
    foreach ($slot as $k => $v) {
      if ($k !== '~') {
        $bin = parseBin($this->bin . '~');
        $bin['mt'] = $v->get_weight_data('mt');
        $bin['oh'] = $v->get_weight_data('oh');
        $bin['weight'] = 1;
        $slot['~']->update_item($bin);
      }
    }

    return $slot;
  }

  function get_category()
  {
    return $this->category;
  }

  function get_percentage($what) {
    $slot = $this->slot;
    if (array_key_exists('~', $this->slot)) {
      $slot_T    = $this->reupdate_swl();
      $slot      = array();
      $slot['~'] = $slot_T['~'];
    }

    $percentage = array(
        'count' => count($slot),
        'over'  => 0);

    foreach ($slot as $v) {
      $slotpc = $v->get_percentage($what);
      if ($slotpc < 0) {
        $percentage['count'] -= 1;
      } elseif ($slotpc > 100) {
        $percentage['over']  += 1;
      } else {
        if (isset($percentage[ $slotpc ])) {
          $percentage[ $slotpc ] += 1;
        } else {
          $percentage[ $slotpc ]  = 1;
        }
      }
    }

    return $percentage;
  }
}

class Lane
{
  public $bin;
  public $stack;

  public $sorting;

  function __construct()
  {
    $this->bin     = '';
    $this->stack   = array();
    $this->sorting = '';
  }

  function update_swl($v)
  {
    $this->bin = $v['group'] . ($v['group_id'] == '0' ? '' : $v['group_id']) . $v['lane'];
    if (!is_null($this->stack) && array_key_exists($v['stack'], $this->stack)) {
      $this->stack[$v['stack']]->update_swl($v);
    } else {
      $item = new Stack;
      $item->update_swl($v);
      $this->stack[$v['stack']] = $item;
    }
  }
  function update_item($v)
  {
    $this->bin = $v['group'] . ($v['group_id'] == '0' ? '' : $v['group_id']) . $v['lane'];
    if (!is_null($this->stack) && array_key_exists($v['stack'], $this->stack)) {
      $this->stack[$v['stack']]->update_item($v);
    } else {
      $item = new Stack;
      $item->update_item($v);
      $this->stack[$v['stack']] = $item;
    }
  }

  function echo_data()
  {
    $sorted = $this->stack;
    if ($this->sorting === '>') {
      ksort($sorted);
    } elseif ($this->sorting === '<') {
      ksort($sorted);
      $sorted = array_reverse($sorted);
    }

    $sorted2 = $sorted;
    $first = array_shift($sorted2);
    $sorted2 = $sorted;
    $last  = end($sorted2);
    $first = $first->bin;
    $last  = $last->bin;

    $str = '';

    $str .= '<div class="stack-container">';
    if ($first == $last) {
      $str .= '<p class="lane-label">' . $first . '</p>';
    } else {
      $str .= '<p class="lane-label">' . $first . ' &ndash; ' . $last . '</p>';
    }

    $str .= '<div class="stack-container-wrapper">';
    foreach ($sorted as $v) {
      $str .= '<div class="stack-table well">';
      $str .= '<div class="well well-small well-white">';

      $str .= $v->echo_data();

      $str .= '</div>';
      $str .= '</div>';
    }
    $str .= '</div>';

    $str .= '<div class="clear"></div>';
    $str .= '</div>';
    return $str . "\n";
  }

  function get_categories() {
    $categories = array();
    foreach ($this->stack as $v) {
      $cat = $v->get_category();
      if (!in_array($cat, $categories)) {
        $categories[] = $cat;
      }
    }
    
    return $categories;
  }

  function get_percentage($what) {
    $percentage = array(
        'count' => 0,
        'over'  => 0);

    foreach ($this->stack as $v) {
      $stackpc              = $v->get_percentage($what);
      $percentage['count'] += $stackpc['count'];
      $percentage['over']  += $stackpc['over'];
      unset($stackpc['count']);
      unset($stackpc['over']);

      foreach ($stackpc as $k2 => $v2) {
        if (isset($percentage[ $k2 ])) {
          $percentage[ $k2 ] += $v2;
        } else {
          $percentage[ $k2 ]  = $v2;
        }
      }
    }

    return $percentage;
  }
}

class Group
{
  public $bin;
  public $lane;
  private $sorter;

  function __construct()
  {
    $this->bin    = '';
    $this->lane   = array();
    $this->sorter = new MySorter;
  }

  function update_swl($v)
  {
    $this->bin = $v['group'] . ($v['group_id'] == '0' ? '' : $v['group_id']);
    $switch = $v['group'] . $v['group_id'] . $v['lane'] . ((int)$v['stack'] % 2 ? '_odd' : '_even');
    if (!is_null($this->lane) && array_key_exists($switch, $this->lane)) {
      $this->lane[$switch]->update_swl($v);
    } else {
      $item = new Lane;
      $item->update_swl($v);
      $this->lane[$switch] = $item;
    }
  }
  function update_item($v)
  {
    $this->bin = $v['group'] . ($v['group_id'] == '0' ? '' : $v['group_id']);
    $switch = $v['group'] . $v['group_id'] . $v['lane'] . ((int)$v['stack'] % 2 ? '_odd' : '_even');
    if (!is_null($this->lane) && array_key_exists($switch, $this->lane)) {
      $this->lane[$switch]->update_item($v);
    } else {
      $item = new Lane;
      $item->update_item($v);
      $this->lane[$switch] = $item;
    }
  }

  function echo_data()
  {
    $sorted = $this->lane;
    ksort($sorted);

    $str = '';
    $str .= '<div>';

    $match_at_least_one = false;
    foreach ($this->sorter->sort_list as $k => $v) {
      if ($match_at_least_one && $k === 'hr') {
        $str .= '<hr />';
      } else {
        foreach ($sorted as $k2 => $v2) {
          if ($k === $k2) {
            $match_at_least_one = true;
            $v2->sorting = $v;
            $str .= '<div class="lane">' . $v2->echo_data() . '</div>';
            unset($sorted[$k2]);
            break;
          }
        }
      }
    }

    if ($match_at_least_one && count($sorted) != 0) {
      $str .= '<hr />';
      $str .= '<hr />';
    }

    foreach ($sorted as $v) {
      $str .= '<div class="lane">' . $v->echo_data() . '</div>';
    }
    $str .= '</div>';

    return $str . "\n" . "\n" . "\n";
  }

  function get_percentage($what) {
    $percentage = array(
        'count' => 0,
        'over'  => 0);

    foreach ($this->lane as $v) {
      $lanepc              = $v->get_percentage($what);
      $percentage['count'] += $lanepc['count'];
      $percentage['over']  += $lanepc['over'];
      unset($lanepc['count']);
      unset($lanepc['over']);

      foreach ($lanepc as $k2 => $v2) {
        if (isset($percentage[ $k2 ])) {
          $percentage[ $k2 ] += $v2;
        } else {
          $percentage[ $k2 ]  = $v2;
        }
      }
    }

    return $percentage;
  }

  function get_categories() {
    $categories = array();

    foreach ($this->lane as $v) {
      $cat = $v->get_categories();

      foreach ($cat as $v2) {
        if (!in_array($v2, $categories)) {
          $categories[] = $v2;
        }
      }
    }

    return $categories;
  }

  function echo_legend() {
    $categories = $this->get_categories();
    $str = '';
    if (count($categories)) {
      $str .= <<<HD
<div id="legend" class="navbar-inner">
  <p>Legend:</p>
  <ul> 
    <li class="well well-small item-regular"><label><input id="filter-legend-regular" type="checkbox" checked="checked" data-toFilter="item-regular" /> Stock Item</label></li>
HD;
    foreach ($categories as $v) {
      if ($v != '') {
        $str .= "    " . '<li class="well well-small item-' . $v . '"><label><input id="filter-legend-regular" type="checkbox" checked="checked" data-toFilter="item-' . $v . '" /> ' . switchCategoryName($v) . '</label></li>' . "\n";
      }
    }
      $str .= <<<HD
  </ul>
</div>
<div id="legend-helper"></div>
HD;
    }

    return $str;
  }

  function convert_percentage($what) {
    $percent  = $this->get_percentage($what);
    $count    = $percent['count'];
    $percent2 = array(
        '10'    => 0,
        '50'    => 0,
        '85'    => 0,
        'over'  => $percent['over']
    );
    unset($percent['count']);
    unset($percent['over']);

//echo "<pre>"; //var_dump($percent);
//echo $percent['count'] . ' - ' . (array_sum($percent) - $percent['count']);

    foreach ($percent as $k => $v) {
      if ($k < 10) {
        $percent2['10'] += $v;
      } elseif ($k < 50) {
        $percent2['50'] += $v;
      } elseif ($k < 85) {
        $percent2['85'] += $v;
      } else {
        $percent2['over'] += $v;
      }
    }

//echo "<pre>"; //var_dump($percent2);

    $percent = array();
    foreach ($percent2 as $k => $v) {
      if ($count == 0) {
        $percent[$k] = 200;
      } else {
        $percent[$k] = round($v * 100 / $count, 1);
      }
    }
    
    return array('p1' => $percent, 'p2' => $percent2);
  }

  function echo_percentage($where = 'slot', $wheres = null) {
    $mt = $this->convert_percentage('mt');
    $oh = $this->convert_percentage('oh');
    $p = $mt;

    $wheres = $wheres == null ? ($where . 's') : $wheres;
    $str  = '<div class="menu-group-view">';
    $str .= '<div class="well-small">';
    $str .= '<table class="trakindo-table table table-fit table-condensed table-bordered table-hover table-striped">';

    $str .= '<thead>';
    $str .= '<tr>';
    $str .= '<th colspan="4" class="center">Weight % of SWL</th>';
    $str .= '</tr>';
    $str .= '<tr class="center">';
    $str .= '<th width="' . round($p['p1']['10'])   . '%" data-width-mt="' . round($mt['p1']['10'])   . '" data-width-oh="' . round($oh['p1']['10'])   . '"><span style="vertical-align:.05em">&lt;</span>10<small style="vertical-align:.05em">%</small></th>';
    $str .= '<th width="' . round($p['p1']['50'])   . '%" data-width-mt="' . round($mt['p1']['50'])   . '" data-width-oh="' . round($oh['p1']['50'])   . '">10<span style="vertical-align:.05em">&#8209;</span>50<small style="vertical-align:.05em">%</small></th>';
    $str .= '<th width="' . round($p['p1']['85'])   . '%" data-width-mt="' . round($mt['p1']['85'])   . '" data-width-oh="' . round($oh['p1']['85'])   . '">50<span style="vertical-align:.05em">&#8209;</span>85<small style="vertical-align:.05em">%</small></th>';
    $str .= '<th width="' . round($p['p1']['over']) . '%" data-width-mt="' . round($mt['p1']['over']) . '" data-width-oh="' . round($oh['p1']['over']) . '"><span style="vertical-align:.05em">over</span></th>';
    $str .= '</tr>';
    $str .= '</thead>';

    $str .= '<tfoot>';
    $str .= '<tr>';
    $str .= '<th colspan="4" class="center"># of ' . $wheres . '</th>';
    $str .= '</tr>';
    $str .= '</tfoot>';


    $str .= '<tbody>';
    $str .= '<tr class="center">';
    $str .= '<td data-td-over-4="0" data-title-mt="below 10% SWL (' . $mt['p2']['10']   . ' ' . ($mt['p2']['10']   == 1 ? $where : $wheres) . ')" data-title-oh="below 10% SWL (' . $oh['p2']['10']   . ' ' . ($oh['p2']['10']   == 1 ? $where : $wheres) . ')" data-display-mt="' . $mt['p1']['10']   . '" data-display-oh="' . $oh['p1']['10']   . '">' . $p['p1']['10']   . '%</td>';
    $str .= '<td data-td-over-4="1" data-title-mt="below 50% SWL (' . $mt['p2']['50']   . ' ' . ($mt['p2']['50']   == 1 ? $where : $wheres) . ')" data-title-oh="below 50% SWL (' . $oh['p2']['50']   . ' ' . ($oh['p2']['50']   == 1 ? $where : $wheres) . ')" data-display-mt="' . $mt['p1']['50']   . '" data-display-oh="' . $oh['p1']['50']   . '">' . $p['p1']['50']   . '%</td>';
    $str .= '<td data-td-over-4="3" data-title-mt="below 85% SWL (' . $mt['p2']['85']   . ' ' . ($mt['p2']['85']   == 1 ? $where : $wheres) . ')" data-title-oh="below 85% SWL (' . $oh['p2']['85']   . ' ' . ($oh['p2']['85']   == 1 ? $where : $wheres) . ')" data-display-mt="' . $mt['p1']['85']   . '" data-display-oh="' . $oh['p1']['85']   . '">' . $p['p1']['85']   . '%</td>';
    $str .= '<td data-td-over-4="5" data-title-mt="overloaded ('    . $mt['p2']['over'] . ' ' . ($mt['p2']['over'] == 1 ? $where : $wheres) . ')" data-title-oh="overloaded ('    . $oh['p2']['over'] . ' ' . ($oh['p2']['over'] == 1 ? $where : $wheres) . ')" data-display-mt="' . $mt['p1']['over'] . '" data-display-oh="' . $oh['p1']['over'] . '">' . $p['p1']['over'] . '%</td>';
    $str .= '</tr>';

    $str .= '<tr class="center">';
    $str .= '<td data-td-over-4="0" data-title-mt="below 10% SWL (' . $mt['p2']['10']   . ' ' . ($mt['p2']['10']   == 1 ? $where : $wheres) . ')" data-title-oh="below 10% SWL (' . $oh['p2']['10']   . ' ' . ($oh['p2']['10']   == 1 ? $where : $wheres) . ')" data-display-mt="' . $mt['p2']['10']   . '" data-display-oh="' . $oh['p2']['10']   . '">' . $p['p2']['10']   . '</td>';
    $str .= '<td data-td-over-4="1" data-title-mt="below 50% SWL (' . $mt['p2']['50']   . ' ' . ($mt['p2']['50']   == 1 ? $where : $wheres) . ')" data-title-oh="below 50% SWL (' . $oh['p2']['50']   . ' ' . ($oh['p2']['50']   == 1 ? $where : $wheres) . ')" data-display-mt="' . $mt['p2']['50']   . '" data-display-oh="' . $oh['p2']['50']   . '">' . $p['p2']['50']   . '</td>';
    $str .= '<td data-td-over-4="3" data-title-mt="below 85% SWL (' . $mt['p2']['85']   . ' ' . ($mt['p2']['85']   == 1 ? $where : $wheres) . ')" data-title-oh="below 85% SWL (' . $oh['p2']['85']   . ' ' . ($oh['p2']['85']   == 1 ? $where : $wheres) . ')" data-display-mt="' . $mt['p2']['85']   . '" data-display-oh="' . $oh['p2']['85']   . '">' . $p['p2']['85']   . '</td>';
    $str .= '<td data-td-over-4="5" data-title-mt="overloaded ('    . $mt['p2']['over'] . ' ' . ($mt['p2']['over'] == 1 ? $where : $wheres) . ')" data-title-oh="overloaded ('    . $oh['p2']['over'] . ' ' . ($oh['p2']['over'] == 1 ? $where : $wheres) . ')" data-display-mt="' . $mt['p2']['over'] . '" data-display-oh="' . $oh['p2']['over'] . '">' . $p['p2']['over'] . '</td>';
    $str .= '</tr>';
    $str .= '</tbody>';

    $str .= '</table>';
    $str .= '</div>';
    $str .= '</div>';

    return $str;
  }
}



class Warehouse
{
  public $group;
  public $display;
  private $wh_group;
  private $swl;
  private $bin;

  function __construct($wh_group)
  {
    $wh_group = strtoupper($wh_group);
    if (preg_match('/^W[123][ABC]$/', $wh_group)) {
      $this->wh_group = substr($wh_group, 1, 2);
      $this->swl = new SWL;
      $this->bin = new Bin;
      $this->group = new Group;
      $this->update_swl($this->swl->swl);
      $this->update_item($this->bin->bin);
      
      return true;
    } else {
      return false;
    }
  }

  private function group_match($bin)
  {
    switch ($this->wh_group) {
      case '1A': return preg_match('/^A[12]/',  $bin);
      case '1B': return preg_match('/^B1[^K]/', $bin);
      case '1C': return preg_match('/^C1/',     $bin);
      case '2C': return preg_match('/^C[KLR]/', $bin);
      case '3B': return preg_match('/^B1K/',    $bin);
      case '3C': return preg_match('/^CH/',     $bin);
      default  : return false;
    }
  }

  private function update_swl($swl)
  {
    foreach ($swl as $val) {
      if ($this->group_match($val['bin'])) {
        $this->group->update_swl( $val );
      }
    }
  }

  private function update_item($bin)
  {
    foreach ($bin as $val) {
      if ($this->swl->swl_exist($val['bin'])) {
        if ($this->group_match($val['bin'])) {
          $this->group->update_item( $val );
        }
      }
    }
  }

  function display($set, $what) {
    switch ($set) {
      case 'set'  : $this->display[$what] = 0; break;
      case 'unset': if (isset($this->display[$what])) { unset($this->display[$what]); } break;
    }
  }

  function echo_data($what, $display = null)
  {
    $this->display('set', 'total');
    if ($display == null) { $display = $this->display; }

    return '' . $this->group->echo_data(strtolower($what), $display);
  }

  function echo_legend() {
    return $this->group->echo_legend();
  }
  function echo_percentage() {
    switch ($this->wh_group) {
      case '1A': return $this->group->echo_percentage('drawer');
      case '1B': return $this->group->echo_percentage('shelf', 'shelves');
      case '1C': return $this->group->echo_percentage('beam');
      case '2C': return $this->group->echo_percentage('beam');
      case '3B': return $this->group->echo_percentage('shelf', 'shelves');
      case '3C': return $this->group->echo_percentage('beam');
    }
  }
}

?>
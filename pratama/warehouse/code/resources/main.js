String.prototype.trim = function() {
	return this.replace(/^\s+|\s+$/g, '');
}
String.prototype.ltrim = function() {
	return this.replace(/^\s+/, '');
}
String.prototype.rtrim = function() {
	return this.replace(/\s+$/, '');
}
String.prototype.lpad = function(str, length) {
	var ret = this;
  while (ret.length < length) { ret = str + ret; }
  return ret;
}
String.prototype.rpad = function(str, length) {
	var ret = this;
  while (ret.length < length) { ret = ret + str; }
  return ret;
}

function switchBinColor(percent)
{
  var r = 0;
  var g = 0;
  var b = (80).toString(16);
  var p = percent / 100;

  if (p <= 0)
  {
    r = (200).toString(16);
    g = (255).toString(16);
    b = (200).toString(16);
  }
  else if (p <= 0.5)
  {
    r = (Math.round(408 * p)).toString(16);
    g = (255).toString(16);
  }
  else if (p <= 0.625)
  {
    r = (Math.round(408 * p)).toString(16);
    g = (255).toString(16);
  }
  else if (p <= 1)
  {
    r = (255).toString(16);
    g = (Math.round(102 + 408 * (1 - p))).toString(16);
  }
  else
  {
    r = (220).toString(16);
    g = (0).toString(16);
    b = (30).toString(16);
  }
  return '#' + r.lpad('0', 2) + g.lpad('0', 2) + b.lpad('0', 2);
}

function reformatWeightForEcho(number, abs)
{
  var num2 = Math.abs(number/1000);
  var unit = '';
  if (num2 == 0) {
  } else if (num2 < 1) {
    number *= 1000;
    unit    = ' g';
  } else if (num2 < 1000) {
    unit = ' kg';
  } else {
    number /= 1000;
    unit    = ' T';
  }

  num2 = Math.round(number / 10, 2) / 100;
  if (abs) { return '' + Math.abs(num2) + unit; } else { return '' + num2 + unit; }
}

function switchUnit(test) {
  if (/ton|t/i.test(test)) {
    return ' * 1000 * 1000';
  } else if (/kg|kilo|kilogram|k/i.test(test)) {
    return ' * 1000';
  } else {
    return '';
  }
}

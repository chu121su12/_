<?php include_once('./include/head.php'); ?>
<div id="content-wrapper" class="_page-home container-fluid well">
  <div id="content-wrapper-img"></div>
  <div id="content-wrapper2">
    <ul id="navi" class="nav nav-tabs">
      <li class="active"><a href="#">Home</a></li>
      <li class="append-left"><a href="./whs.php?w=w1">Warehouse 1</a></li>
      <li class="dropdown">
        <a class="dropdown-toggle"
           data-toggle="dropdown"
           href="#"><b class="caret"></b></a>
        <ul class="dropdown-menu pull-right">
          <li><a href="./warehouse.php?w=w1a">Cabinet</a></li>
          <li><a href="./warehouse.php?w=w1b">Shelf</a></li>
          <li><a href="./warehouse.php?w=w1c">Rack</a></li>
        </ul>
      </li>

      <li class="append-left"><a href="./whs.php?w=w2">Warehouse 2</a></li>
      <li class="dropdown">
        <a class="dropdown-toggle"
           data-toggle="dropdown"
           href="#"><b class="caret"></b></a>
        <ul class="dropdown-menu pull-right">
          <li><a href="./warehouse.php?w=w2c">Rack</a></li>
        </ul>
      </li>

      <li class="append-left"><a href="./whs.php?w=w3">Warehouse 3</a></li>
      <li class="dropdown">
        <a class="dropdown-toggle"
           data-toggle="dropdown"
           href="#"><b class="caret"></b></a>
        <ul class="dropdown-menu pull-right">
          <li><a href="./warehouse.php?w=w3b">Shelf</a></li>
          <li><a href="./warehouse.php?w=w3c">Rack</a></li>
        </ul>
      </li>
    </ul>
    <div class="well well-small">
      <dl class="dl-horizontal">
        <dt>Everything:</dt>     <dd>Display all.</dd>
        <dt>&lt;10%:</dt>        <dd>Display total weight in particular location that less than 10% of SWL.</dd>
        <dt>10-50%:</dt>         <dd>Display total weight in particular location between 10-50% of SWL.</dd>
        <dt>50-85%:</dt>         <dd>Display total weight in particular location between 50-85% of SWL.</dd>
        <dt>&gt;85%:</dt>        <dd>Display total weight in particular location that greater than 85% (over).</dd>
        <dt>total weight:</dt>   <dd>Display total weight in particular location.</dd>
        <dt>remaining:</dt>      <dd>Display the remaining capacity.</dd>
        <dt>total (max/TA):</dt> <dd>Display the <strong>maximum weight</strong> of particular location.</dd>
        <dt>OH:</dt>             <dd>Display weight based on quantity <strong>on hand</strong>.</dd>
      </dl>
    </div>
  </div>
</div>

<script src="./resources/jquery.min.js" type="text/javascript" charset="utf-8"></script>
<script src="./resources/bootstrap.min.js" type="text/javascript" charset="utf-8"></script>
<script>
$('.dropdown-toggle').dropdown()
</script>
<?php include_once('./include/foot.php'); ?>
<?php
ini_set('memory_limit', '-1');
ini_set('max_execution_time', 0);

if (!isset($_GET['w']) || isset($_GET['w']) && !preg_match('/^W[123][ABC]$/i', $_GET['w'])) { $_GET['w'] = 'w1a'; }
if (!isset($_GET['m']) || isset($_GET['m']) && !preg_match('/^(mt|oh)$/', $_GET['m'])) { $_GET['m'] = 'mt'; }
if (!isset($_GET['filter_weight_remaining'])) { $_GET['filter_weight_remaining'] = 'total'; }
if (!isset($_GET['weight_remaining'])) { $_GET['weight_remaining'] = 'total'; }

require_once './functions.php';

$wh = new Warehouse($_GET['w']);
switch (strtoupper($_GET['w'])) {
  case 'W1A': $title = 'Warehouse 1 &mdash; Cabinet'; break;
  case 'W1B': $title = 'Warehouse 1 &mdash; Shelf';   break;
  case 'W1C': $title = 'Warehouse 1 &mdash; Rack';    break;
  case 'W2C': $title = 'Warehouse 2 &mdash; Rack';    break;
  case 'W3B': $title = 'Warehouse 3 &mdash; Shelf';   break;
  case 'W3C': $title = 'Warehouse 3 &mdash; Rack';    break;
  default   : $title = 'empty';                       break;
}

?><?php include_once('./include/head.php'); ?>

<div class="_page-warehouse container-fluid well">
  <ul id="navi" class="nav nav-tabs">
    <li><a href="./main.php">Home</a></li>
    <li class="<?php echo substr(strtoupper($_GET['w']), 0, 2) == 'W1' ? 'active active-clickable ' : ''; ?>append-left"><a href="./whs.php?w=w1">Warehouse 1</a></li>
    <li class="dropdown">
      <a class="dropdown-toggle"
         data-toggle="dropdown"
         href="#"><b class="caret"></b></a>
      <ul class="dropdown-menu pull-right">
        <li><a href="./warehouse.php?w=w1a">Cabinet</a></li>
        <li><a href="./warehouse.php?w=w1b">Shelf</a></li>
        <li><a href="./warehouse.php?w=w1c">Rack</a></li>
      </ul>
    </li>

    <li class="<?php echo substr(strtoupper($_GET['w']), 0, 2) == 'W2' ? 'active active-clickable ' : ''; ?>append-left"><a href="./whs.php?w=w2">Warehouse 2</a></li>
    <li class="dropdown">
      <a class="dropdown-toggle"
         data-toggle="dropdown"
         href="#"><b class="caret"></b></a>
      <ul class="dropdown-menu pull-right">
        <li><a href="./warehouse.php?w=w2c">Rack</a></li>
      </ul>
    </li>

    <li class="<?php echo substr(strtoupper($_GET['w']), 0, 2) == 'W3' ? 'active active-clickable ' : ''; ?>append-left"><a href="./whs.php?w=w3">Warehouse 3</a></li>
    <li class="dropdown">
      <a class="dropdown-toggle"
         data-toggle="dropdown"
         href="#"><b class="caret"></b></a>
      <ul class="dropdown-menu pull-right">
        <li><a href="./warehouse.php?w=w3b">Shelf</a></li>
        <li><a href="./warehouse.php?w=w3c">Rack</a></li>
      </ul>
    </li>
  </ul>

  <div id="filter-wrapper" class="well well-small well-white">
    <!--h3><?php echo substr($title, 20); ?></h3-->
    <h3><?php echo $title; ?></h3>
    <ul id="filter-list">
      <li>
        use
        <label class="well well-small">
          <input id="switch-mt" type="radio" name="m" value="mt"<?php echo ($_GET['m'] == 'mt' ? ' checked="checked"' : ''); ?> />
          TOTAL (<sub>max</sub>\<sup>TA</sup>)
        </label>
        or
        <label class="well well-small">
          <input id="switch-oh" type="radio" name="m" value="oh"<?php echo ($_GET['m'] == 'oh' ? ' checked="checked"' : ''); ?> />
          OH
        </label>
      </li>
      <li>
        display
        <label class="well well-small">
          <input id="switch-weight" type="radio" name="weight_remaining" value="total"<?php echo ($_GET['weight_remaining'] == 'total' ? ' checked="checked"' : ''); ?> />
          total weight
        </label>
        or
        <label class="well well-small">
          <input id="switch-remaining" type="radio" name="weight_remaining" value="remaining"<?php echo ($_GET['weight_remaining'] == 'remaining' ? ' checked="checked"' : ''); ?> />
          remaining
        </label>
      </li>
      <li>
        and show:
    		<ul id="main-filter">
          <li>
    			  <label class="well well-small">
              <input id="filter-none" type="radio" name="filter" data-searchValue="" checked="checked" />
              everything
            </label>
          </li>
          <li>
      			<label class="well well-small">
              <input id="filter-10" type="radio" name="filter" data-searchValue="<10%" />
              below 10% SWL
            </label>
          </li>
          <li>
      			<label class="well well-small">
              <input id="filter-50" type="radio" name="filter" data-searchValue="10%-50%" />
              10% to 50% SWL
            </label>
          </li>
          <li>
      			<label class="well well-small">
              <input id="filter-85" type="radio" name="filter" data-searchValue="50%-85%" />
              50% to 85% SWL
            </label>
          </li>
          <li>
      			<label class="well well-small">
              <input id="filter-over" type="radio" name="filter" data-searchValue=">=85%" />
              overloaded
            </label>
          </li>
    		</ul>
      </li>
    </ul>
  	<!--span id="test"></span-->
    <div class="clear"></div>
  </div>

  <div class="content well well-white">
<?php
$wh->display('set', $_GET['weight_remaining']);
echo $wh->echo_legend();
echo $wh->echo_data($_GET['m']);
?>
  </div>
</div>

<script src="./resources/jquery.min.js" type="text/javascript" charset="utf-8"></script>
<script src="./resources/bootstrap.min.js" type="text/javascript" charset="utf-8"></script>
<script src="./resources/main.js" type="text/javascript" charset="utf-8"></script>
<script>
function redo_td(td) {
  td.attr('data-total-main', td.attr('data-total-' + td.attr('data-display-mt-oh')));
  td.html(reformatWeightForEcho( td.attr('data-total-' + td.attr('data-display-mt-oh')) ));

  if (td.hasClass('switch-title')) {
    var each       = td.attr('data-each-' + td.attr('data-display-mt-oh'));
    var each_array = each.split(';');
    var str        = '';
    for (i=0; i<each_array.length; ++i) {
      var slot = each_array[i].split(':');
      if (str == '') {
        str += '[' + slot[0] + ': ' + reformatWeightForEcho(slot[1]) + ']';
      } else {
        str += '; [' + slot[0] + ': ' + reformatWeightForEcho(slot[1]) + ']';
      }
    }

    td.attr('title', 'Total each: ' + str);
  }

  if (td.hasClass('switch-swl')) {
    if (td.attr('data-filter-weight') == 'remaining') {
      td.attr('data-weight-to-filter', td.attr('data-swl') - td.attr('data-total-' + td.attr('data-display-mt-oh')));
    } else {
      td.attr('data-weight-to-filter', td.attr('data-total-' + td.attr('data-display-mt-oh')));
    }

    if (td.attr('data-display-weight') == 'remaining') {
      td.html( reformatWeightForEcho(td.attr('data-swl') - td.attr('data-total-' + td.attr('data-display-mt-oh'))) );
    } else {
      td.html( reformatWeightForEcho(td.attr('data-total-' + td.attr('data-display-mt-oh'))) );
    }
  }

  if (td.hasClass('switch-percent')) {
    td.attr('data-percent-to-filter', td.attr('data-percent-' + td.attr('data-display-mt-oh')));
  }
}

function do_filter(force, val) {
  if (typeof(force) === 'undefined') force = false;
  if (typeof(val) === 'undefined') val = $('#main-filter input:checked').attr('data-searchValue');

  val = val.replace(/\s+/g, '');

  $('.stack-table:not(.on-on-legend)').hide();

  if (val == '') {
    $('.lane, .stack-table.on-on-legend, tr').show();
  } else if (force || val != $(this).attr('data-past')) {

    var noun        = /^over|overload|overloaded|empty$/i;
    var range       = /^(\d+|\d*[.,]\d+)(grams?|gr|kgs?|kilos?|kilograms?|tons?|[kgt%])?-(\d+|\d*[.,]\d+)(grams?|gr|kgs?|kilos?|kilograms?|tons?|[kgt%])?$/i;
    var single      = /^([<=>]=?)?(\d+|\d*[.,]\d+)(grams?|gr|kgs?|kilos?|kilograms?|tons?|[kgt%])?$/i;
    var eval_str    = '';
    var is_range    = false;
    var is_percent  = false;

    if (noun.test(val)) {
      if (/over|overload|overloaded/i.test(val)) {
        eval_str  = ' >= 85';
        is_percent = true;

      } else if (val.toLowerCase() == 'empty') {
        eval_str  = ' == 0';
      }

    } else if (range.test(val)) {
      var matches = range.exec(val);
      var unit1   = switchUnit(matches[2]);
      var unit2   = switchUnit(matches[4]);
      is_percent  = matches[2] == '%' && matches[4] == '%';
      if (eval(matches[1] + unit1) < eval(matches[3] + unit2)) {
        eval_str = ' >= (' + matches[1] + unit1 + ') && (' + matches[3] + unit2 + ') > ';
        is_range = true;
      } else if (eval(matches[1] + unit1) > eval(matches[3] + unit2)) {
        eval_str = ' >= (' + matches[3] + unit2 + ') && (' + matches[1] + unit1 + ') > ';
        is_range = true;
      } else {
        eval_str = ' == (' + matches[1] + unit1 + ')';
      }
    } else if (single.test(val)) {
      var matches = single.exec(val);
      var unit    = switchUnit(matches[3]);
      is_percent  = matches[3] == '%';
      eval_str    = ' ' + (!matches[1] || matches[1] == '=' ? '==' : matches[1]) + ' (' + matches[2] + unit + ')';
    }

    //$('#test').html('' + (is_percent ? '%':'') + (is_range ? 'r: ':'s: ') + eval_str + '');

    if (eval_str) {
      $('.lane, .stack-table.on-on-legend').show();
      $('tr.shelf-total-header').hide();
      
      $('td:nth-child(2)').each(function(){
        if (!(typeof($(this).attr('data-weight-to-filter')) === 'undefined')) {
          var evl = ''
          if (is_percent) {
            if ($(this).attr('data-percent-to-filter') < 0) {
              evl = 'false';
            } else {
              if (is_range) {
                evl = $(this).attr('data-percent-to-filter') + eval_str + $(this).attr('data-percent-to-filter');
              } else {
                evl = $(this).attr('data-percent-to-filter') + eval_str;
              }
            }
          } else {
            if (is_range) {
              evl = $(this).attr('data-weight-to-filter') + eval_str + $(this).attr('data-weight-to-filter');
            } else {
              evl = $(this).attr('data-weight-to-filter') + eval_str;
            }
          }

          if (eval( evl )) {
            $(this).parent().show();
          } else {
            $(this).parent().hide();
          }
        } else {
          $(this).parent().hide();
        }
      });
    }
  }

  $('tbody').each(function(){
    if ($(this).children('tr:visible').size() == 0) {
      $(this).closest('.stack-table').hide();
    } else {
      $(this).closest('.stack-table').show();
    }
  });

  $('.stack-container-wrapper').each(function(){
    if ($(this).find('.stack-table:visible').size() == 0) {
      $(this).closest('.lane').hide();
    } else {
      $(this).closest('.lane').show();
    }
  });
  $(this).attr('data-past', val);

  $('.stack-container-wrapper').each(function(){
    var height = 0;
    $(this).find('.trakindo-table').each(function(){
      height = Math.max(height, $(this).height());
    }).closest('.stack-table').css('height',2 + height + 'px');
  });
}

$(function(){
  $(window).scroll(function(){
    $("#filter-wrapper").css("top",Math.max(0,$(this).scrollTop()<0?130:130-$(this).scrollTop()));
  });

  $('caption').closest('.stack-table').addClass('on-on-legend');

  $('td.switch-data').each(function(){
    $(this).attr('data-total-main', $(this).attr('data-total-mt'));
    $(this).attr('data-weight-to-filter', $(this).attr('data-total-mt'));
    $(this).attr('data-percent-to-filter', $(this).attr('data-percent-mt'));
  });
  $('td.switch-data').attr('data-filter-weight', 'weight');
  $('td.switch-data').attr('data-display-weight', 'weight');
  $('td.switch-data').attr('data-display-mt-oh', 'mt');
  do_filter(true);

  $('#legend input').change(function(){
    if ($(this).is(':checked')) {
      $('caption.' + $(this).attr('data-toFilter')).closest('.stack-table').addClass('on-on-legend');
    } else {
      $('caption.' + $(this).attr('data-toFilter')).closest('.stack-table').removeClass('on-on-legend');
    }
    do_filter(true);
  });

  $('#filter-weight').click(function(){
    $('td.switch-data').attr('data-filter-weight', 'weight');
    $('td.switch-data').each(function(){
      redo_td( $(this) );
    });
    do_filter(true);
  });

  $('#filter-remaining').click(function(){
    $('td.switch-data').attr('data-filter-weight', 'remaining');
    $('td.switch-data').each(function(){
      redo_td( $(this) );
    });
    do_filter(true);
  });

  $('#switch-weight').click(function(){
    $('td.switch-data').attr('data-display-weight', 'weight');
    $('td.switch-data').each(function(){
      $(this).closest('table').find('thead th.cell-total').html('Weight');
      redo_td( $(this) );
    });
    do_filter(true);
  });

  $('#switch-remaining').click(function(){
    $('td.switch-data').attr('data-display-weight', 'remaining');
    $('td.switch-data').each(function(){
      $(this).closest('table').find('thead th.cell-total').html('Remaining');
      redo_td( $(this) );
    });
    do_filter(true);
  });

  $('#switch-mt').click(function(){
    $('td.switch-data').attr('data-display-mt-oh', 'mt');
    $('td.switch-data').each(function(){
      redo_td( $(this) );

      if ($(this).hasClass('switch-swl')) {
        var bg = switchBinColor($(this).attr('data-percent-to-filter'));
        $(this).prev().css('background', bg);
        if (bg == '#dc001e') {
          $(this).prev().css('color', 'white');
        } else {
          $(this).prev().css('color', null);
        }
      }
    });
    do_filter(true);
  });

  $('#switch-oh').click(function(){
    $('td.switch-data').attr('data-display-mt-oh', 'oh');
    $('td.switch-data').each(function(){
      redo_td( $(this) );

      if ($(this).hasClass('switch-swl')) {
        var bg = switchBinColor($(this).attr('data-percent-to-filter'));
        $(this).prev().css('background', bg);
        if (bg == '#dc001e') {
          $(this).prev().css('color', 'white');
        } else {
          $(this).prev().css('color', null);
        }
      }
    });
    do_filter(true);
  });

  $('#main-filter input').bind('change', function(){
	  do_filter();
  });
  $('#switch-weight, #switch-mt').trigger('click');

});
</script>
<?php include_once('./include/foot.php'); ?>
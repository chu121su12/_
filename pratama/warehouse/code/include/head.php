<!DOCTYPE html>
<html lang="en">
<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo isset($title) && $title != '' ? $title : 'SWL Monitoring System'; ?></title>
<link rel="stylesheet" href="./resources/bootstrap.css" type="text/css" />
<!--link rel="stylesheet" href="./resources/bootstrap-responsive.css" type="text/css" /-->
<link rel="stylesheet" href="./resources/bootstrap-me.css" type="text/css" />
<link rel="stylesheet" href="./resources/main.css" type="text/css" />

</head>
<body><div class="_body-div">

<div id="main-header" class="navbar">
  <div class="navbar-inner">
    <span class="logo">
      <a href="./main.php"><img class="well-small" src="./resources/Trakindo-CAT.jpg" /></a>
    </span>
    <span class="brand">SWL Monitoring System</span>
  </div>
</div>

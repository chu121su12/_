<?php
$mytime = microtime(true); // Gets microseconds

ini_set('memory_limit', '-1');
ini_set('max_execution_time', 0);

if (!isset($_GET['w']) || isset($_GET['w']) && !preg_match('/^W[123]$/i', $_GET['w'])) { $_GET['w'] = 'w1'; }

require_once './functions.php';

switch (strtoupper($_GET['w'])) {
  case 'W1': $title = 'Warehouse 1'; break;
  case 'W2': $title = 'Warehouse 2'; break;
  case 'W3': $title = 'Warehouse 3'; break;
  default  : $title = 'empty';       break;
}

?><?php include_once('./include/head.php'); ?>
<div class="_page-sub-home container-fluid well">
  <div id="content-wrapper2">
    <ul id="navi" class="nav nav-tabs">
      <li><a href="./main.php">Home</a></li>
      <li class="<?php echo strtoupper($_GET['w']) == 'W1' ? 'active ' : ''; ?>append-left"><a href="<?php echo strtoupper($_GET['w']) == 'W1' ? '#' : './whs.php?w=w1'; ?>">Warehouse 1</a></li>
      <li class="dropdown">
        <a class="dropdown-toggle"
           data-toggle="dropdown"
           href="#"><b class="caret"></b></a>
        <ul class="dropdown-menu pull-right">
          <li><a href="./warehouse.php?w=w1a">Cabinet</a></li>
          <li><a href="./warehouse.php?w=w1b">Shelf</a></li>
          <li><a href="./warehouse.php?w=w1c">Rack</a></li>
        </ul>
      </li>

      <li class="<?php echo strtoupper($_GET['w']) == 'W2' ? 'active ' : ''; ?>append-left"><a href="<?php echo strtoupper($_GET['w']) == 'W2' ? '#' : './whs.php?w=w2'; ?>">Warehouse 2</a></li>
      <li class="dropdown">
        <a class="dropdown-toggle"
           data-toggle="dropdown"
           href="#"><b class="caret"></b></a>
        <ul class="dropdown-menu pull-right">
          <li><a href="./warehouse.php?w=w2c">Rack</a></li>
        </ul>
      </li>

      <li class="<?php echo strtoupper($_GET['w']) == 'W3' ? 'active ' : ''; ?>append-left"><a href="<?php echo strtoupper($_GET['w']) == 'W3' ? '#' : './whs.php?w=w3'; ?>">Warehouse 3</a></li>
      <li class="dropdown">
        <a class="dropdown-toggle"
           data-toggle="dropdown"
           href="#"><b class="caret"></b></a>
        <ul class="dropdown-menu pull-right">
          <li><a href="./warehouse.php?w=w3b">Shelf</a></li>
          <li><a href="./warehouse.php?w=w3c">Rack</a></li>
        </ul>
      </li>
    </ul>

    <div id="filter-wrapper" class="well well-small well-white">
      <ul id="filter-list">
        <li>
          use
          <label class="well well-small">
            <input id="switch-mt" type="radio" name="m" value="mt"<?php echo ($_GET['m'] == 'mt' ? ' checked="checked"' : ''); ?> />
            TOTAL (<sub>max</sub>\<sup>TA</sup>)
          </label>
          or
          <label class="well well-small">
            <input id="switch-oh" type="radio" name="m" value="oh"<?php echo ($_GET['m'] == 'oh' ? ' checked="checked"' : ''); ?> />
            OH
          </label>
        </li>
      </ul>
    	<!--span id="test"></span-->
      <div class="clear"></div>
    </div>

<?php if (strtoupper($_GET['w']) == 'W1'): ?>
    <div class="row-fluid">
      <div class="well well-small well-white">
        <h4><a href="./warehouse.php?w=w1a">Cabinet</a></h4>
<?php
  $wh = new Warehouse('W1A');
  echo $wh->echo_percentage() . "\n";
  unset($wh);
?>
      </div>
    </div>

    <div class="row-fluid">
      <div class="well well-small well-white">
        <h4><a href="./warehouse.php?w=w1b">Shelf</a></h4>
<?php
  $wh = new Warehouse('W1B');
  echo $wh->echo_percentage() . "\n";
  unset($wh);
?>
      </div>
    </div>

    <div class="row-fluid">
      <div class="well well-small well-white">
        <h4><a href="./warehouse.php?w=w1c">Rack</a></h4>
<?php
  $wh = new Warehouse('W1C');
  echo $wh->echo_percentage() . "\n";
  unset($wh);
  ?>
      </div>
    </div>
<?php elseif (strtoupper($_GET['w']) == 'W2'): ?>
    <div class="row-fluid">
      <div class="well well-small well-white">
        <h4><a href="./warehouse.php?w=w2c">Rack</a></h4>
<?php
  $wh = new Warehouse('W2C');
  echo $wh->echo_percentage() . "\n";
  unset($wh);
?>
      </div>
    </div>
<?php elseif (strtoupper($_GET['w']) == 'W3'): ?>
    <div class="row-fluid">
      <div class="well well-small well-white">
        <h4><a href="./warehouse.php?w=w3b">Shelf</a></h4>
<?php
  $wh = new Warehouse('W3B');
  echo $wh->echo_percentage() . "\n";
  unset($wh);
?>
      </div>
    </div>

    <div class="row-fluid">
      <div class="well well-small well-white">
        <h4><a href="./warehouse.php?w=w3c">Rack</a></h4>
<?php
  $wh = new Warehouse('W3C');
  echo $wh->echo_percentage() . "\n";
  unset($wh);
?>
      </div>
    </div>
<?php endif; ?>
  </div>
</div>

<script src="./resources/jquery.min.js" type="text/javascript" charset="utf-8"></script>
<script src="./resources/bootstrap.min.js" type="text/javascript" charset="utf-8"></script>
<script src="./resources/main.js" type="text/javascript" charset="utf-8"></script>
<script>
$(function(){
  $(window).scroll(function(){
    $("#filter-wrapper").css("top",Math.max(0,$(this).scrollTop()<0?130:130-$(this).scrollTop()));
  });
  $('td[data-td-over-4]').each(function(){
    $(this).css('background',switchBinColor($(this).attr('data-td-over-4')/4.0*100));
  });

  $('#switch-mt').click(function(){
    $('thead th[width]').each(function(){
      $(this).attr('width', $(this).attr('data-width-mt') + '%');
    });
    $('tr:first-child td[data-td-over-4]').each(function(){
      $(this).text($(this).attr('data-display-mt') + '%');
      $(this).attr('title',$(this).attr('data-title-mt'));
    });
    $('tr:last-child td[data-td-over-4]').each(function(){
      $(this).text($(this).attr('data-display-mt'));
      $(this).attr('title',$(this).attr('data-title-mt'));
    });
  });

  $('#switch-oh').click(function(){
    $('thead th[width]').each(function(){
      $(this).attr('width', $(this).attr('data-width-oh') + '%');
    });
    $('tr:first-child td[data-td-over-4]').each(function(){
      $(this).text($(this).attr('data-display-oh') + '%');
      $(this).attr('title',$(this).attr('data-title-oh'));
    });
    $('tr:last-child td[data-td-over-4]').each(function(){
      $(this).text($(this).attr('data-display-oh'));
      $(this).attr('title',$(this).attr('data-title-oh'));
    });
  });

  $('#switch-weight, #switch-mt').trigger('click');
});
</script>
<?php include_once('./include/foot.php'); ?>